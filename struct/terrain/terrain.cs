using System.Collections.Generic;
public struct Terrain{
    int Type;
    string Description;
    List<ElementModifier> Modifiers;
    int Turns;
}