public class Card{
    private int Id;    
    public double Attack;
    public double Defense;
    public int Rarity; // 0/12
    public int Type;
    public string Description;
    
    public void incrementAtk(double amount){
        Attack += amount;
        return;
    }

    public void incrementDef(double amount){
        Defense += amount;
        return;
    }

}